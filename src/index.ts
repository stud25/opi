import { parseRows } from "./utils/parse-rows";
import { readFileLines } from "./utils/read-file-rows";

import { findLowPrecipitationDays } from "./tasks/find-low-precipitation-days.task";
import { findRainyDays } from "./tasks/find-rainy-days.task";

readFileLines("./data.txt", parseRows).then((rows) => {
  const rainyDays = findRainyDays(rows);
  const lowPrecipitationDays = findLowPrecipitationDays(rows);

  console.log("rainy days:", rainyDays);
  console.log("low precipitation days:", lowPrecipitationDays);
});
