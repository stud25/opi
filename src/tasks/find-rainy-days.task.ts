import type { Row } from "../types/row.type";

export function findRainyDays(rows: Row[]): string[] {
  const rainyDays: string[] = [];

  rows.forEach(({ status, date }) => {
    if (status.includes("дождь")) {
      rainyDays.push(date);
    }
  });

  return rainyDays;
}
