import type { Row } from "../types/row.type";

export function findLowPrecipitationDays(rows: Row[]): string[] {
  const rainyDays: string[] = [];

  rows.forEach(({ precipitation, date }) => {
    if (precipitation < 1.5) {
      rainyDays.push(date);
    }
  });

  return rainyDays;
}
