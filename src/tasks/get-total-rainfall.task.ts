import type { Row } from "../types/row.type";

export function getTotalRainfall(rows: Row[], monthValue: number): number {
  const filteredRows = rows.filter((row) => {
    const [_, monthString] = row.date.split(".");
    const monthNumber = Number(monthString);

    if (isNaN(monthNumber) || monthNumber != monthValue) {
      return false;
    }

    return true;
  });

  return filteredRows.reduce<number>((accamulator, { precipitation }) => accamulator + precipitation, 0);
}
