export type Row = {
  date: string;
  precipitation: number;
  status: string;
};
