import type { Row } from "../types/row.type";

export function parseRows(rows: string[]): Row[] {
  return rows.reduce<Row[]>((accamulator, row) => {
    const [date, precipitation, ...status] = row.split(" ");

    const precipitationNumber = Number(precipitation);

    if (isNaN(precipitationNumber)) {
      return accamulator;
    }

    accamulator.push({
      date,
      precipitation: precipitationNumber,
      status: status.join(" "),
    });

    return accamulator;
  }, []);
}
